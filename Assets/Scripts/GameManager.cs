﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [Header("Generation")]
    [SerializeField] private GameObject dayBackgroundPrefab = null;
    [SerializeField] private GameObject nightBackgroundPrefab = null;
    [SerializeField] private Generator dayPipeGeneratorPrefab = null;
    [SerializeField] private Generator nightPipeGeneratorPrefab = null;
    [SerializeField] private List<Generator> generators = null;
    [SerializeField] private Bird[] birds = null;

    [Header("UI")]
    [SerializeField] private TextMeshProUGUI scoreText = null;
    [SerializeField] private Fader screenFlashPanel = null;
    [SerializeField] private Fader readyMessage = null;
    [SerializeField] private Fader gameOverMessage = null;
    [SerializeField] private ScoreBoard scoreBoard = null;

    [Header("Audio")]
    [SerializeField] private AudioClip tapSound = null;
    [SerializeField] private AudioClip gameOverSound = null;

    private AudioSource audioSource;
    private const string KeyBestScore = "BestScore";
    private const string KeyGivesScore = "GivesScore";
    private Coroutine gameOverHandle;

    private int bestScore = -1;
    private int score = -1;
    private Bird birdInPlay;
    private bool pickDay;
    private bool gameStarted = false;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();

        scoreBoard.gameObject.SetActive(false);
        bestScore = PlayerPrefs.HasKey(KeyBestScore) ? PlayerPrefs.GetInt(KeyBestScore) : 0;
        UpdateScore();

        pickDay = Random.Range(0, 2) == 0;

        Instantiate(pickDay ? dayBackgroundPrefab : nightBackgroundPrefab, Vector3.zero, Quaternion.identity);

        int pickBird = Random.Range(0, birds.Length);
        birdInPlay = Instantiate(birds[pickBird], birds[pickBird].transform.position, Quaternion.identity);
        birdInPlay.onGameOver += GameOver;
        birdInPlay.onUpdateScore += UpdateScore;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && !gameStarted)
        {
            StartGame();
            gameStarted = true;
        }

        void StartGame()
        {
            scoreText.enabled = true;
            readyMessage.StartFade();
            birdInPlay.StartBird();
            generators.Add(Instantiate(pickDay ? dayPipeGeneratorPrefab : nightPipeGeneratorPrefab, Vector3.zero, Quaternion.identity));
        }
    }

    private void UpdateScore()
    {
        score++;
        scoreText.text = $"{score}";

        if (score > bestScore)
        {
            bestScore = score;
        }
    }

    private void GameOver()
    {
        if (gameOverHandle == null)
        {
            gameOverHandle = StartCoroutine(Co_GameOver());
        }

        IEnumerator Co_GameOver()
        {
            screenFlashPanel.onFadeEnded += () =>
            {
                gameOverMessage.StartFade();
                scoreText.enabled = false;
            };

            gameOverMessage.onFadeEnded += () =>
            {
                scoreBoard.gameObject.SetActive(true);
                scoreBoard.Setup(score, bestScore);
            };

            screenFlashPanel.StartFade();

            audioSource.PlayOneShot(gameOverSound);
            birdInPlay.StopBird();

            foreach (var generator in generators)
            {
                generator.StopScrolling();
            }

            foreach (var collider in FindObjectsOfType<Collider2D>())
            {
                if (collider.gameObject.layer == LayerMask.NameToLayer(KeyGivesScore))
                {
                    collider.enabled = false;
                }
            }

            PlayerPrefs.SetInt(KeyBestScore, bestScore);
            yield return new WaitForSeconds(3f);
            yield return new WaitUntil(() =>
            {
                if (Input.GetMouseButtonDown(0))
                {
                    audioSource.PlayOneShot(tapSound);
                    return true;
                }

                return false;
            });
            yield return new WaitForSeconds(tapSound.length);
            yield return SceneManager.LoadSceneAsync(0, LoadSceneMode.Single);
        }
    }
}
