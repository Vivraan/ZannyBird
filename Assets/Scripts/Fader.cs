﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class Fader : MonoBehaviour
{
    [SerializeField] private float flashSpeed = 10f;
    [SerializeField] private Color fadeFrom = Color.white;
    [SerializeField] private Color fadeTo = new Color(1f, 1f, 1f, 0f);
    private Image image = null;
    private bool startFade = false;

    public Action onFadeEnded;

    private void Start()
    {
        image = GetComponent<Image>();
        image.color = fadeFrom;
    }

    private void Update()
    {
        if (startFade)
        {
            image.color = Color.Lerp(image.color, fadeTo, flashSpeed * Time.deltaTime);

            if (image.color == fadeTo)
            {
                onFadeEnded?.Invoke();
            }
        }
    }

    public void StartFade()
    {
        image.enabled = true;
        startFade = true;
    }
}
