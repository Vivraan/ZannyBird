﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class Generator : MonoBehaviour
{
    [SerializeField] private GameObject prefabToGenerate = null;
    [SerializeField] private float xOffset = 0f;
    [SerializeField] private float zOrder = 0f;
    [SerializeField] private int generateCount = 1;
    [SerializeField] private float scrollSpeed = 1f;
    [SerializeField, FormerlySerializedAs("yAbsoluteBias")] private float yBias = 0f;
    [SerializeField, FormerlySerializedAs("yRandomViewportOffset"), Range(0f, 0.5f)] private float ySpawnRandomViewportOffset = 0f;
    [SerializeField] private Vector3 spawnViewportCoords = new Vector3(1.1f, 0.5f, 0f);
    [SerializeField] private Vector3 despawnViewportCoords = new Vector3(-1.1f, 0.5f, 0f);

    private Vector3 spawnPoint;
    private Vector3 despawnPoint;

    private GameObject[] objects;
    private bool canScroll = true;

    // Start is called before the first frame update
    void Start()
    {
        spawnPoint = Camera.main.ViewportToWorldPoint(spawnViewportCoords);
        despawnPoint = Camera.main.ViewportToWorldPoint(despawnViewportCoords);

        objects = new GameObject[generateCount];

        for (int i = 0; i < generateCount; i++)
        {
            objects[i] = Instantiate(prefabToGenerate, spawnPoint, Quaternion.identity);

            var reposition = objects[i].transform.position;
            reposition.x += xOffset * i;
            reposition.y = yBias + GetRandomYOffset();
            reposition.z = zOrder;

            objects[i].transform.position = reposition;
        }
    }

    // Update is called once per frame
    void Update()
    {
        spawnPoint = Camera.main.ViewportToWorldPoint(spawnViewportCoords);
        despawnPoint = Camera.main.ViewportToWorldPoint(despawnViewportCoords);

        for (int i = 0; i < objects.Length; i++)
        {
            var position = objects[i].transform.position;
            if (canScroll)
            {
                position.x -= scrollSpeed * Time.deltaTime;
            }

            if (position.x <= despawnPoint.x)
            {
                position.x = objects[Mod(i - 1, objects.Length)].transform.position.x + xOffset;
                position.y = yBias + GetRandomYOffset();
            }

            objects[i].transform.position = position;
        }
    }

    private float GetRandomYOffset()
    {
        if (ySpawnRandomViewportOffset != 0f)
        {
            var extent = Camera.main.ViewportToWorldPoint(new Vector3(0f, ySpawnRandomViewportOffset, 0f));
            return Random.Range(-extent.y, extent.y);
        }
        else
        {
            return 0f;
        }
    }

    private int Mod(in int x, in int m)
    {
        return (x % m + m) % m;
    }

    public void StopScrolling()
    {
        canScroll = false;
    }
}
