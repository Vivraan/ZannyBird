﻿using System;
using System.Collections;
using UnityEngine;

public class Bird : MonoBehaviour
{
    [SerializeField] private GameObject groundPrefab = null;
    [SerializeField] private float jumpForce = 1f;
    [SerializeField] private float tapCooldownDuration = 0.1f;
    [SerializeField] private float upwardVelocityLimit = 50f;
    [SerializeField] private AudioClip hitSound = null;
    [SerializeField] private AudioClip scoreSound = null;
    [SerializeField] private AudioClip jumpSound = null;

    public Action onGameOver;
    public Action onUpdateScore;

    private float tapCooldownTimer;
    private new Rigidbody2D rigidbody;
    private new SpriteRenderer renderer;
    private AudioSource audioSource;
    private bool canJump = true;
    private float birdFaceAngle = 0f;
    const string KeyKills = "Kills";
    const string KeyGivesScore = "GivesScore";
    const string KeyGround = "Ground";

    private void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        renderer = GetComponent<SpriteRenderer>();
        audioSource = GetComponent<AudioSource>();
        tapCooldownTimer = tapCooldownDuration;
    }

    private void FixedUpdate()
    {
        Jump();

        void Jump()
        {
            tapCooldownTimer += Time.fixedDeltaTime;

            if (canJump && Input.GetMouseButton(0) && tapCooldownTimer >= tapCooldownDuration)
            {
                bool isVisible = Camera.main.IsObjectVisible(renderer);
                if (rigidbody.velocity.y <= upwardVelocityLimit && isVisible)
                {
                    rigidbody.velocity = Vector2.zero;
                    rigidbody.AddForce(new Vector2(0f, jumpForce));
                    audioSource.PlayOneShot(jumpSound);
                }

                tapCooldownTimer = 0;
            }
        }
    }

    private void Update()
    {
        RotateBird();

        void RotateBird()
        {
            float groundY = groundPrefab.transform.position.y;
            // v^2 = u^2 + 2as, u = 0
            var maxDownwardVelocity = Mathf.Sqrt(2 * Physics2D.gravity.y * groundY);

            if (!rigidbody.constraints.HasFlag(RigidbodyConstraints2D.FreezePositionY))
            {
                var angle = Scale(rigidbody.velocity.y, -90, 90, -maxDownwardVelocity, maxDownwardVelocity);
                birdFaceAngle = Mathf.Clamp(angle, -90, 45);
            }

            transform.rotation = Quaternion.Euler(0f, 0f, birdFaceAngle);
        }

        float Scale(in float x, in float xMin, in float xMax, in float yMin, in float yMax)
        {
            return (xMax - xMin) / (yMax - yMin) * x;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer(KeyKills))
        {
            audioSource.PlayOneShot(hitSound);
            StartCoroutine(AudioEndCallback(hitSound.length));
            onGameOver?.Invoke();
        }
        else if (other.gameObject.layer == LayerMask.NameToLayer(KeyGivesScore))
        {
            audioSource.PlayOneShot(scoreSound);
            onUpdateScore?.Invoke();
        }

        if (other.CompareTag(KeyGround))
        {
            rigidbody.constraints = RigidbodyConstraints2D.FreezeAll;
        }

        IEnumerator AudioEndCallback(float clipLength)
        {
            yield return new WaitForSeconds(clipLength);
            audioSource.volume = 0f;
        }
    }

    public void StartBird()
    {
        rigidbody.simulated = true;
    }

    public void StopBird()
    {
        canJump = false;
        GetComponent<Animator>().enabled = false;
    }
}
