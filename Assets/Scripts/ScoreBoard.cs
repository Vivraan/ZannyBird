﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ScoreBoard : MonoBehaviour
{
    [SerializeField] private Color noMedalColor = Color.clear;
    [SerializeField] private Color bronzeColor = Color.clear;
    [SerializeField] private Color silverColor = Color.clear;
    [SerializeField] private Color goldColor = Color.clear;
    [SerializeField] private Color platinumColor = Color.clear;
    [SerializeField] private TextMeshProUGUI scoreText = null;
    [SerializeField] private TextMeshProUGUI bestScoreText = null;
    [SerializeField] private Image medalImage = null;

    private Animator animator;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void Setup(in int score, in int bestScore)
    {
        scoreText.text = $"{score}";
        bestScoreText.text = $"{bestScore}";

        if (score >= 40)
        {
            medalImage.color = platinumColor;
        }
        else if (score >= 30)
        {
            medalImage.color = goldColor;
        }
        else if (score >= 20)
        {
            medalImage.color = silverColor;
        }
        else if (score >= 10)
        {
            medalImage.color = bronzeColor;
        }
        else
        {
            medalImage.color = noMedalColor;
        }

        if (animator)
        {
            animator.enabled = true;
        }
    }
}
